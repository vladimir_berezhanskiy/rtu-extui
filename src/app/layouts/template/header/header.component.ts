import {Component, OnInit, Output, EventEmitter } from '@angular/core';
import {AuthenticationService} from '../../../shared/services/auth.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  avatar: string;
  constructor(private AuthService: AuthenticationService) {
  }
  @Output() public sidenavToggle = new EventEmitter();

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

  ngOnInit(): void {
    this.avatar = this.AuthService.currentUserValue.body.role_name;
    //console.log(this.avatar);
  }

  logout(): void {
    this.AuthService.logout();
  }
}
