import { Routes } from '@angular/router';


// tslint:disable-next-line:variable-name
export const CommonLayout_ROUTES: Routes = [
  /*dashboard*/
  {
    path: 'dashboard',
    loadChildren: () => import('../../dashboard/dashboard.module').then(m => m.DashboardModule),
  },
 /*apps*/
  {
    path: 'apps',
    children: [
      {
        path: '',
        loadChildren: () => import('../../apps/apps.module').then(m => m.AppsModule),
      }
    ],
}];

