import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {LoadingService} from '../shared/services/loading.service';

@Component({
  selector: 'app-layouts',
  templateUrl: './layouts.component.html',
  styleUrls: ['./layouts.component.scss']
})
export class LayoutsComponent implements OnInit, AfterViewChecked {
  public loading$ = this.loader._loading.asObservable();
  // tslint:disable-next-line:variable-name
  constructor(public loader: LoadingService, private cdr: ChangeDetectorRef) {
/*    this.loader._loading.subscribe((v) => {
      console.log(v);
    });*/
  }


  ngOnInit(): void {
    /*this.loading$.next(this.loader.loading$);*/
  }

  ngAfterViewChecked(): void {
    this.cdr.detectChanges();
  }
}
