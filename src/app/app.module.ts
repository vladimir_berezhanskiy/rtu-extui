import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { LayoutsComponent } from './layouts/layouts.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TemplateModule } from './layouts/template/template.module';
import {SharedModule} from './shared/shared.module';
import {ToastrModule} from 'ngx-toastr';
import {WelcomComponent} from './pages/welcom/welcom.component';
import {NgxSpinnerModule, NgxSpinnerService} from 'ngx-spinner';
import {MAT_DATE_LOCALE} from "@angular/material/core";


@NgModule({
  declarations: [
    AppComponent,
    LayoutsComponent,
    WelcomComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        FlexLayoutModule,
        TemplateModule,
        SharedModule,
        ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: environment.production,
            // Register the ServiceWorker as soon as the app is stable
            // or after 30 seconds (whichever comes first).
            registrationStrategy: 'registerWhenStable:30000'
        }),
        ToastrModule.forRoot(),
        NgxSpinnerModule,
    ],
  providers: [NgxSpinnerService, { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }],
  exports: [NgxSpinnerModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
