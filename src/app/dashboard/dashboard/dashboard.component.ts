import {Component, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {HttpClient} from '@angular/common/http';
import {LoadingService} from '../../shared/services/loading.service';
import {AppsService} from '../../shared/services/app.service';
import {AuthenticationService} from '../../shared/services/auth.service';
import {forkJoin} from "rxjs";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [
    trigger('bodyExpansion', [
      state('collapsed, void', style({ height: '0px', visibility: 'hidden' })),
      state('expanded', style({ height: '*', visibility: 'visible' })),
      transition('expanded <=> collapsed, void => collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
    trigger('myInsertRemoveTrigger', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('1000ms', style({ opacity: 1 })),
      ]),
      transition(':leave', [
        animate('1000ms', style({ opacity: 0 }))
      ])
    ]),
  ]
})
export class DashboardComponent implements OnInit {
  state = 'expanded';
  subscribe: any;
  users: any;
  orders: any;
  numbers: any;
  rules: any;


  constructor(private http: HttpClient, public loader: LoadingService,
              private appsService: AppsService,
              private Auth: AuthenticationService) {
  }

  ngOnInit(): void {
    this.subscribe = forkJoin([this.appsService.countUsers(), this.appsService.countOrders(),
      this.appsService.countNumbers(), this.appsService.countRules()]).subscribe(result => {
      this.users = result[0].total_count;
      this.orders = result[1].total_count;
      this.numbers = result[2].total_count;
      this.rules = result[3].total_count;
    });
  }

  toggle(): void {
    this.state = this.state === 'expanded' ? 'collapsed' : 'expanded';
  }
}

