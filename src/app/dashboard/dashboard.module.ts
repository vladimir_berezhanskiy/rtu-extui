import {NgModule, Provider} from '@angular/core';
import {CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {FlexModule} from '@angular/flex-layout';
import {HTTP_INTERCEPTORS} from '@angular/common/http';

import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {JwtInterceptor} from '../shared/interceptor/jwt.interceptor';

import {RouterModule} from '@angular/router';

import {NgxSpinnerModule} from 'ngx-spinner';
import {AppsService} from '../shared/services/app.service';



const INTERCEPTOR_PROVIDER: Provider = {
  provide: HTTP_INTERCEPTORS,
  multi: true,
  useClass: JwtInterceptor
};


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    DashboardRoutingModule,
    MatCardModule,
    MatFormFieldModule,
    MatExpansionModule,
    MatTableModule,
    MatIconModule,
    MatMenuModule,
    MatDividerModule,
    MatButtonModule,
    FlexModule,
    MatProgressSpinnerModule,
    RouterModule,
    SharedModule,
    NgxSpinnerModule,
  ],
  exports: [RouterModule],
  declarations: [
    DashboardComponent,
  ],
  providers: [INTERCEPTOR_PROVIDER, AppsService],
})
export class DashboardModule { }
