import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AuthGuards} from '../shared/guards/auth.guard';



const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuards],
    component: DashboardComponent,
    data: {
      title: 'Events Dashboard',
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule { }
