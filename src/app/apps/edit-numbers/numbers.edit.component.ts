import {AfterViewInit, Component, OnInit} from '@angular/core';
import {LoadingService} from '../../shared/services/loading.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AppsService} from '../../shared/services/app.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { MatDatepickerTimeHeaderComponent } from 'mat-datepicker-time-header';
import {MatDatepickerInputEvent} from "@angular/material/datepicker";


@Component({
  templateUrl: './numbers.edit.component.html',
  styleUrls: ['./numbers.edit.component.scss']
})

export class NumbersEditComponent implements OnInit, AfterViewInit {
  timeHeader = MatDatepickerTimeHeaderComponent;
  anydata: [{ id: number; name: string; }] | [];
  id: number;
  value: string;
  desc: string;
  reserved_until: any;
  block_until: any;
  order_id: number;
  in_rr_id: number;
  out_rr_id: number;
  status: number;
  checked: boolean;
  rules: [{ id: number; name: string; contact: string }] | [];
  // tslint:disable-next-line:variable-name
  updateForm: FormGroup;
  constructor(public loader: LoadingService, private activateRoute: ActivatedRoute, private appService: AppsService, private router: Router,
              private fb: FormBuilder) {
  }
  ngAfterViewInit(): void {
  }

  log(val): void {
    console.log(val);
  }

  ngOnInit(): void {
    this.id = this.activateRoute.snapshot.params.id;
    this.updateForm = this.fb.group({
      value: [null, [Validators.required]],
      desc: [null, [Validators.required]],
      reserved_until: [null, [Validators.required]],
      block_until: [null, [Validators.required]],
      order_id: [null, [Validators.required]],
      in_rr_id: [null, [Validators.required]],
      out_rr_id: [null, [Validators.required]],
    });
    this.appService.getOrders(null, null, null, null).subscribe(rules => {
      this.anydata = rules?.list;
    });
    this.appService?.getRules().subscribe(result => {
      this.rules = result?.list;
    });


    if (this.id == null || this.id === undefined) {
      return;
    }
        this.appService.getNumber(this.id).subscribe(result => {
          this.value = result.body.value;
          this.reserved_until = result.body.reserved_until;
          this.block_until = result.body.block_until;
          this.desc = result.body.desc;
          this.status = result.body.status;
          this.order_id = result.body.order_id;
          this.in_rr_id = result.body.in_rr_id;
          this.out_rr_id = result.body.out_rr_id;
          this.updateForm.setValue({
            value: this.value,
            reserved_until: this.reserved_until,
            block_until: this.block_until,
            desc: this.desc,
            order_id: this.order_id,
            in_rr_id: this.in_rr_id,
            out_rr_id: this.out_rr_id
          });
        });
  }
  // tslint:disable-next-line:typedef
  return(): void {
    this.router.navigate(['/apps/', 'numbers']);
  }
  addNumber(): void {
    this.appService.addNumber(
      this.updateForm.value.value,
      this.updateForm.value.desc,
      this.updateForm.value.reserved_until,
      this.updateForm.value.block_until,
      this.updateForm.value.order_id,
      this.updateForm.value.in_rr_id,
      this.updateForm.value.out_rr_id).subscribe(result => {
      //console.log(result);
      this.return();
    });
  }

  updateNumber(): void {
    this.appService.updateNumber(
      +this.id,
      this.updateForm.value.value,
      this.updateForm.value.desc,
      this.updateForm.value.reserved_until,
      this.updateForm.value.block_until,
      this.updateForm.value.order_id,
      this.updateForm.value.in_rr_id,
      this.updateForm.value.out_rr_id).subscribe(result => {
      //console.log(result);
      this.return();
    });
  }
  converttoEpoch(value) {
    if (value) {
      const myDate = Math.floor(new Date(value).getTime() / 1000.0);
      return myDate;
    }
  }

  addEvent(event: MatDatepickerInputEvent<Date>) {
    this.updateForm?.controls['reserved_until']?.setValue(this.converttoEpoch(event?.value));
  }
  addEvent2(event: MatDatepickerInputEvent<Date>) {
    this.updateForm?.controls['block_until']?.setValue(this.converttoEpoch(event?.value));
  }
}
