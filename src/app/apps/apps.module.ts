import {NgModule, Provider} from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AppsRoutingModule } from './apps.routing.module';
import { EventsComponent } from './users/events.component';
import {AboutComponent} from './about/about.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatNativeDateModule, MatOptionModule} from '@angular/material/core';
import {MatIconModule} from '@angular/material/icon';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatPaginatorModule} from '@angular/material/paginator';
import {AppsService} from '../shared/services/app.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {JwtInterceptor} from '../shared/interceptor/jwt.interceptor';
import {MatSortModule} from '@angular/material/sort';
import { DialogBoxComponent } from './dialog-box/dialog-box.component';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { CustomPaginator } from './users/CustomPaginatorConfiguration';
import {NgxSpinnerModule} from 'ngx-spinner';
import {EditComponent} from './edit-users/edit.component';
import {MatCardModule} from '@angular/material/card';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {FlexModule} from '@angular/flex-layout';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatToolbarModule} from '@angular/material/toolbar';
import { NumbersComponent } from './numbers/numbers.component';
import { RulesComponent } from './rules/rules.component';
import { OrdersComponent } from './orders/orders.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {OrdersEditComponent} from './edit-orders/orders.edit.component';
import {NumbersEditComponent} from './edit-numbers/numbers.edit.component';
import {EpochDatePipe} from '../shared/pipes/date.epoch.pipe';
import {DateEpochPipe} from '../shared/pipes/epoch.date.pipe';
import {RulesEditComponent} from "./edit-rules/rules.edit.component";

const INTERCEPTOR_PROVIDER: Provider = {
  provide: HTTP_INTERCEPTORS,
  multi: true,
  useClass: JwtInterceptor
};

@NgModule({
    imports: [
        SharedModule,
        ReactiveFormsModule,
        AppsRoutingModule,
        FlexModule,
        MatFormFieldModule,
        MatSlideToggleModule,
        MatOptionModule,
        MatIconModule,
        MatSelectModule,
        MatTableModule,
        MatButtonModule,
        MatDialogModule,
        MatDatepickerModule,
        MatInputModule,
        MatCheckboxModule,
        MatPaginatorModule,
        MatSortModule,
        NgxSpinnerModule,
        MatCardModule,
        MatProgressSpinnerModule,
        MatToolbarModule,
        MatTooltipModule,
        MatNativeDateModule,
        MatDatepickerModule,
    ],
  declarations: [EventsComponent, AboutComponent, DialogBoxComponent, EditComponent, NumbersComponent, RulesComponent, OrdersComponent,
    OrdersEditComponent, NumbersEditComponent, EpochDatePipe, DateEpochPipe, DateEpochPipe, RulesEditComponent],
  providers: [INTERCEPTOR_PROVIDER, AppsService, {provide: MatPaginatorIntl, useValue: CustomPaginator()}],
})

export class AppsModule {}
