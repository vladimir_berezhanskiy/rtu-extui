import {AfterViewInit, Component, OnInit} from '@angular/core';
import {LoadingService} from '../../shared/services/loading.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AppsService} from '../../shared/services/app.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';


@Component({
  templateUrl: './orders.edit.component.html',
  styleUrls: ['./orders.edit.component.scss']
})
export class OrdersEditComponent implements OnInit, AfterViewInit {
  id: number;
  name: string;
  customer_name: string;
  desc: string;
  status: number;
  checked: boolean;
  // tslint:disable-next-line:variable-name
  updateForm: FormGroup;
  constructor(public loader: LoadingService, private activateRoute: ActivatedRoute, private appService: AppsService, private router: Router) {
    this.id = activateRoute.snapshot.params.id;
    this.updateForm = new FormGroup({
      customer_name: new FormControl('', [Validators.required]),
      name: new FormControl('', [Validators.required]),
      desc: new FormControl('', [Validators.minLength(0)]),
      /*status: new FormControl('', [Validators.nullValidator]),*/
/*      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),*/
      /*userPhone: new FormControl('', Validators.pattern("[0-9]{10}"))*/
    });
  }
  loading$ = this.loader.loading$;
  ngAfterViewInit(): void {
  }

  ngOnInit(): void {
    if (this.id == null || this.id === undefined) {
      this.status = 0;
      return;
    }

    this.appService.getOrder(this.id).subscribe(result => {
      this.customer_name = result.body.customer_name;
      this.name = result.body.name;
      this.desc = result.body.desc;
      this.status = result.body.status;
      switch (this.status) {
        case 1: {
          this.checked = true;
          break;
        }
        case 0: {
          this.checked = false;
          break;
        }
      }
      this.updateForm.setValue({
        customer_name: this.customer_name,
        name: this.name,
        desc: this.desc,
      });
    });
  }
  // tslint:disable-next-line:typedef

  return(): void {
    this.router.navigate(['/apps/', 'orders']);
  }
  addOrders(): void {
      this.appService.addOrders(
        this.updateForm.value.customer_name,
        this.updateForm.value.name,
        this.updateForm.value.desc,
        this.status).subscribe(result => {
        this.return();
      });
    }

  updateOrders(): void {
    this.appService.updateOrders(
      +this.id,
      this.updateForm.value.customer_name,
      this.updateForm.value.name,
      this.updateForm.value.desc,
      this.status).subscribe(result => {
      this.return();
    });
  }

  chekedStatus(): void {
    if (this.status === 1) {
      this.status = 0;
      this.checked = false;
    } else {
      this.status = 1;
      this.checked = true;
    }
    //console.log(this.status);
  }
}
