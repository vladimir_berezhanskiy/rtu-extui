import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {NumbersDataList} from '../../shared/interfaces/user.interfaces';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {AppsService} from '../../shared/services/app.service';
import {LoadingService} from '../../shared/services/loading.service';
import {AuthenticationService} from '../../shared/services/auth.service';
import {DialogBoxComponent} from '../dialog-box/dialog-box.component';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.scss']
})
export class RulesComponent implements OnInit, AfterViewInit {
  subscribe: any;
  loading$ = this.loader.loading$;
  displayedColumns: string[] = ['id', 'name', 'contact', 'action'];
  dataSource: MatTableDataSource<NumbersDataList>;

  @ViewChild(MatTable, {static: true}) table: MatTable<any>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(public dialog: MatDialog, private appsService: AppsService, public loader: LoadingService, public AuthService: AuthenticationService) {
    this.dataSource = new MatTableDataSource();
  }

  openDialog(action, obj): void {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result?.event === 'Delete') {
        /*//console.log(result.data);*/
        this.deleteRowData(result?.data);
      }
    });
  }

  deleteRowData(row_obj): void {
    /*    this.dataSource.data = this.dataSource.data.filter((value, key) => {
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          return value.id !== row_obj.id;
        });*/
    this.appsService.deleteRule(row_obj.id).subscribe(result => {
      this.refresh();
      this.table.renderRows();
    });
  }

  refresh(): void {
    if (this.paginator) { this.paginator._changePageSize(this.paginator.pageSize); }
  }

  applyFilter(filterValue?: string): void {
    filterValue = filterValue?.trim().toLowerCase(); // Remove whitespace
    /*filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches*/
    setTimeout(() => {
      this.dataSource.filter = filterValue;
    }, 1000);
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.subscribe = this.appsService.getRules().subscribe(result => {
      // @ts-ignore
      this.dataSource.data = result.list || [];
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.sortingDataAccessor = (data, attribute) => data[attribute];
    });
  }
}
