import {AfterViewInit, Component, OnInit} from '@angular/core';
import {LoadingService} from '../../shared/services/loading.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AppsService} from '../../shared/services/app.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';


@Component({
  templateUrl: './rules.edit.component.html',
  styleUrls: ['./rules.edit.component.scss']
})
export class RulesEditComponent implements OnInit, AfterViewInit {
  id: number;
  name: string;
  contact: string;
  updateForm: FormGroup;
  constructor(public loader: LoadingService, private activateRoute: ActivatedRoute,
              private appService: AppsService, private router: Router) {
    this.id = activateRoute.snapshot.params.id;
    this.updateForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      contact: new FormControl('', [Validators.minLength(0)]),
    });
  }
  ngAfterViewInit(): void {
  }

  ngOnInit(): void {
    if (this.id == null || this.id === undefined) {
      return;
    }

    this.appService.getRule(this.id).subscribe(result => {
      // @ts-ignore
      this.name = result.body.name;
      // @ts-ignore
      this.contact = result.body.contact;
      this.updateForm.setValue({
        name: this.name,
        contact: this.contact,
      });
    });
  }

  return(): void {
    this.router.navigate(['/apps/', 'rules']);
  }

  addRules(): void {
      this.appService.addRule(
        this.updateForm.value.name,
        this.updateForm.value.contact).subscribe(result => {
        this.return();
      });
  }

  updateRule(): void {
    this.appService.updateRule(
      +this.id,
      this.updateForm.value.name,
      this.updateForm.value.contact).subscribe(result => {
      this.return();
    });
  }

  deleteRule(): void {
    this.appService.deleteRule(
      +this.id).subscribe(result => {
      this.return();
    });
  }
}
