import {AfterViewInit, Component, OnInit} from '@angular/core';
import {LoadingService} from '../../shared/services/loading.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AppsService} from '../../shared/services/app.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';


@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit, AfterViewInit {
  pass: string;
  login: string;
  name: string;
  surname: string;
  email: string;
  id: number;
  status: number;
  checked: boolean;
  // tslint:disable-next-line:variable-name
  role_id: number;
  updateForm: FormGroup;
  constructor(public loader: LoadingService, private activateRoute: ActivatedRoute, private appService: AppsService, private router: Router) {
    this.id = activateRoute.snapshot.params.id;
    this.updateForm = new FormGroup({
      email: new FormControl('', [Validators.email]),
      login: new FormControl('', [Validators.email]),
      password: new FormControl('', [Validators.minLength(8)]),
      name: new FormControl('', [Validators.minLength(3)]),
      surname: new FormControl('', [Validators.minLength(0)]),
      role_id: new FormControl('', [Validators.minLength(1)]),
      /*status: new FormControl('', [Validators.nullValidator]),*/
/*      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),*/
      /*userPhone: new FormControl('', Validators.pattern("[0-9]{10}"))*/
    });
  }
  loading$ = this.loader.loading$;
  ngAfterViewInit(): void {
  }

  ngOnInit(): void {
    if (this.id == null || this.id === undefined) {
      return;
    }
    this.appService.getUser(this.id).subscribe(result => {
      this.login = result.body.login;
      this.name = result.body.name;
      this.surname = result.body.surname;
      this.email = result.body.email;
      this.status = result.body.status;
      this.role_id = result.body.role_id;
      switch (this.status) {
        case 1: {
          this.checked = true;
          break;
        }
        case 0: {
          this.checked = false;
          break;
        }
      }
      this.updateForm.setValue({
        email: this.email,
        login: this.login,
        name: this.name,
        surname: this.surname,
        password: '',
        role_id: ''
      });
    });
/*    const controls = this.updateForm.controls;
    Object.keys(controls)
      .forEach(controlName => controls[controlName].markAsTouched());
    Object.keys(controls)
      .forEach(controlName => controls[controlName].markAllAsTouched());
    Object.keys(controls)
      .forEach(controlName => controls[controlName].markAsDirty());*/
  }
  // tslint:disable-next-line:typedef
  GeneratePassword() {
    // tslint:disable-next-line:one-variable-per-declaration
    const length = 10,
      charset = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@-#$';
    let res;
    if (window.crypto && window.crypto.getRandomValues) {
      return Array(length)
        .fill(charset)
        .map(x => x[Math.floor(crypto.getRandomValues(new Uint32Array(1))[0] / (0xffffffff + 1) * (x.length + 1))])
        .join('');
    } else {
      res = '';
      for (let i = 0, n = charset.length; i < length; ++i) {
        res += charset.charAt(Math.floor(Math.random() * n));
      }
      return res;
    }
  }

  // tslint:disable-next-line:typedef
  GeneratePwd() {
    if (this.id != null || this.id !== undefined) {
      this.pass = this.GeneratePassword();
      this.updateForm.setValue({
        email: this.email,
        login: this.login,
        name: this.name,
        surname: this.surname,
        password: this.pass,
        role_id: this.role_id
      });
      //console.log(this.updateForm);
      return this.pass;
    } else {
      this.pass = this.GeneratePassword();
      this.updateForm.setValue({
        email: this.updateForm.value.email,
        login: this.updateForm.value.login,
        name: this.updateForm.value.name,
        surname: this.updateForm.value.surname,
        password: this.pass,
        role_id: this.updateForm.value.role_id
      });
      //console.log(this.updateForm);
      return this.pass;
    }
  }

  return(): void {
    this.router.navigate(['/apps/', 'users']);
  }
  saveUser(): void {
    if (this.updateForm.value.password.length === 0) {
      //console.log(this.updateForm.value.password.length);
      this.appService.updateUser(
        +this.id,
        this.updateForm.value.email,
        this.updateForm.value.login,
        this.updateForm.value.name,
        this.updateForm.value.surname,
        this.status).subscribe(result => {
        this.return();
      });
    } else {
      this.appService.updateUser(
        +this.id,
        this.updateForm.value.email,
        this.updateForm.value.login,
        this.updateForm.value.name,
        this.updateForm.value.surname,
        this.status,
        this.updateForm.value.password).subscribe(result => {
        this.return();
      });
    }
  }

  addUser(): void {
    this.appService.addUser(
      this.updateForm.value.login,
      this.updateForm.value.email,
      this.updateForm.value.password,
      this.updateForm.value.name,
      this.status,
      this.updateForm.value.role_id,
      this.updateForm.value.surname
      ).subscribe(result => {
      this.return();
    });
  }

  chekedStatus(): void {
    if (this.status === 1) {
      this.status = 0;
      this.checked = false;
    } else {
      this.status = 1;
      this.checked = true;
    }
  }
}
