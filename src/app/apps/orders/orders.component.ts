import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {NumbersDataList, OrdersDataList} from '../../shared/interfaces/user.interfaces';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {AppsService} from '../../shared/services/app.service';
import {LoadingService} from '../../shared/services/loading.service';
import {AuthenticationService} from '../../shared/services/auth.service';
import {DialogBoxComponent} from '../dialog-box/dialog-box.component';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit, AfterViewInit {

  resultsLength = 0;
  displayedColumns: string[] = ['id', 'customer_name', 'name', 'desc', 'status', 'action'];
  dataSource: MatTableDataSource<OrdersDataList>;
  sortdata = '+';
  public filter: string = '';

  @ViewChild(MatTable, {static: true}) table: MatTable<any>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(public dialog: MatDialog, private appsService: AppsService, public loader: LoadingService) {
    this.dataSource = new MatTableDataSource();
  }

  openDialog(action, obj): void {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result?.event === 'Delete') {
        this.deleteRowData(result?.data);
      }
    });
  }
  deleteRowData(row_obj): void {
/*    this.dataSource.data = this.dataSource.data.filter((value, key) => {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      return value.id !== row_obj.id;
    });*/
    this.appsService.deleteOrder(row_obj.id).subscribe(result => {
      this.refresh();
    });
    this.table.renderRows();
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => {
      this.sortdata = this.sortdata === '+' ? this.sortdata = '-' : this.sortdata = '+';
      this.paginator.pageIndex = 0;
    });
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          // tslint:disable-next-line:no-non-null-assertion
          return this.appsService!.getOrders(this.paginator.pageIndex, this.sortdata, this.paginator
            .pageSize, this.getFilter());
        }),
        map(data => {
          // @ts-ignore
          this.resultsLength = data.total_count;
          return data.list;
        }),
        catchError(() => {
          return observableOf([]);
        })
        // @ts-ignore
      ).subscribe(data => this.dataSource.data = data);
    //console.log(this.getFilter());
  }

  getFilter(){ // формирую фильтр, если он есть
    if (this.filter && this.displayedColumns.length > 0) {
      let filterData = {filter: {fields: [], type: 1}}; // условие между полями; FILTER_TYPE_AND = 0, FILTER_TYPE_OR = 1
      for (let field of this.displayedColumns) {
        filterData['filter']['fields'].push({
          field: field,
          condition_type: 3,
          // CONDITION_TYPE_EQUAL = 0, CONDITION_TYPE_STARTSWITH = 1, CONDITION_TYPE_ENDSWITH = 2, CONDITION_TYPE_LIKE = 3
          value: this.filter
        });
      }
      return filterData;
    } else return {};
  }

  applyFilter(filterValue: string) {
    this.filter = filterValue.trim().toLowerCase(); // Remove whitespace; MatTableDataSource defaults to lowercase matches
    if (this.paginator && this.paginator.pageIndex != 0) this.paginator.pageIndex = 0;
    this.refresh();
  }

  refresh() {
    if (this.paginator) this.paginator._changePageSize(this.paginator.pageSize);
  }

  ngOnInit(): void {
  }
}
