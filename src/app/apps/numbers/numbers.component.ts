import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {NumbersDataList, UsersDataList} from '../../shared/interfaces/user.interfaces';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {AppsService} from '../../shared/services/app.service';
import {LoadingService} from '../../shared/services/loading.service';
import {AuthenticationService} from '../../shared/services/auth.service';
import {DialogBoxComponent} from '../dialog-box/dialog-box.component';
import {identity, merge, Observable, of as observableOf, pipe} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styleUrls: ['./numbers.component.scss']
})
export class NumbersComponent implements OnInit, AfterViewInit {
  subscribe: any;
  displayedColumns: string[] = ['id', 'value', 'desc', 'reserved_until', 'block_until',  'order_id', 'in_rr_id', 'out_rr_id', 'action'];
  dataSource: MatTableDataSource<NumbersDataList>;
  sortdata = '+';
  resultsLength = 0;
  orders: any;
  anydata2$: any;
  public filter: string = '';
  anydata: any;

  @ViewChild(MatTable, {static: true}) table: MatTable<any>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(public dialog: MatDialog, private appsService: AppsService, public loader: LoadingService, public AuthService: AuthenticationService) {
    //console.log('constructor init');
    this.dataSource = new MatTableDataSource();
    this.appsService.getOrders(null, null, null, null).subscribe(rules => {
      this.anydata = rules.list;
      if (this.anydata.length){
        let select_type_obj = {};
        for (var i in this.anydata){
          let t = this.anydata[i];
          select_type_obj[t['id']] = t['name'];
        }
        this.anydata = select_type_obj;
      }
    });
    this.appsService.getRules().subscribe(rules => {
      this.anydata2$ = rules.list;
      if (this.anydata2$.length){
        //console.log('daaaa');
        let select_type_obj = {};
        for (var i in this.anydata2$) {
          let t = this.anydata2$[i];
          select_type_obj[t['id']] = t['name'];
        }
        this.anydata2$ = select_type_obj;
      }
    });
  }

  openDialog(action, obj): void {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data: obj
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result?.event === 'Delete') {
        this.deleteRowData(result?.data);
      }
    });
  }

  deleteRowData(row_obj): void {
/*    this.dataSource.data = this.dataSource.data.filter((value, key) => {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      return value.id !== row_obj.id;
    });*/
    this.appsService.deleteNumber(row_obj.id).subscribe(result => {
      this.refresh();
      this.table.renderRows();
    });
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => {
      this.sortdata = this.sortdata === '+' ? this.sortdata = '-' : this.sortdata = '+';
      this.paginator.pageIndex = 0;
    });
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          // tslint:disable-next-line:no-non-null-assertion
          return this.appsService!.getNumbers(this.paginator.pageIndex, this.sortdata, this.paginator
            .pageSize, this.getFilter());
        }),
        map(data => {
          // @ts-ignore
          this.resultsLength = data.total_count;
          return data.list;
        }),
        catchError(() => {
          return observableOf([]);
        })
        // @ts-ignore
      ).subscribe(data => this.dataSource.data = data);
  }

  getFilter(){ // формирую фильтр, если он есть
    if (this.filter && this.displayedColumns.length > 0) {
      let filterData = {filter: {fields: [], type: 1}}; // условие между полями; FILTER_TYPE_AND = 0, FILTER_TYPE_OR = 1
      for (let field of ['id', 'value']) {
        filterData['filter']['fields'].push({
          field: field,
          condition_type: 3,
          // CONDITION_TYPE_EQUAL = 0, CONDITION_TYPE_STARTSWITH = 1, CONDITION_TYPE_ENDSWITH = 2, CONDITION_TYPE_LIKE = 3
          value: this.filter
        });
      }
      return filterData;
    } else return {};
  }

  applyFilter(filterValue: string): void {
    this.filter = filterValue.trim().toLowerCase();
    if (this.paginator && this.paginator.pageIndex != 0) { this.paginator.pageIndex = 0; }
    this.refresh();
  }

  refresh(): void {
    if (this.paginator) { this.paginator._changePageSize(this.paginator.pageSize); }
  }

  ngOnInit(): void {
  }

  log(val: string): void {
    console.log(val);
  }
}
