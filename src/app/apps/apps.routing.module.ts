import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EventsComponent} from './users/events.component';
import {AuthGuards} from '../shared/guards/auth.guard';
import {EditComponent} from './edit-users/edit.component';
import {NumbersComponent} from './numbers/numbers.component';
import {RulesComponent} from './rules/rules.component';
import {OrdersComponent} from './orders/orders.component';
import {OrdersEditComponent} from "./edit-orders/orders.edit.component";
import {NumbersEditComponent} from "./edit-numbers/numbers.edit.component";
import {RulesEditComponent} from "./edit-rules/rules.edit.component";

const routes: Routes = [
  {
    path: 'users',
    canActivate: [AuthGuards],
    component: EventsComponent,
    data: {
      title: 'Сотрудники',
    }
  },
  {
    path: 'users/edit/:id',
    canActivate: [AuthGuards],
    component: EditComponent,
    data: {
      title: 'users',
    }
  },
  {
    path: 'users/edit',
    canActivate: [AuthGuards],
    component: EditComponent,
    data: {
      title: 'Сотрудники',
    }
  },
  {
    path: 'numbers',
    canActivate: [AuthGuards],
    component: NumbersComponent,
    data: {
      title: 'Номера',
    }
  },
  {
    path: 'numbers/edit/:id',
    canActivate: [AuthGuards],
    component: NumbersEditComponent,
    data: {
      title: 'Номера',
    }
  },
  {
    path: 'numbers/edit',
    canActivate: [AuthGuards],
    component: NumbersEditComponent,
    data: {
      title: 'Номера',
    }
  },
  {
    path: 'rules',
    canActivate: [AuthGuards],
    component: RulesComponent,
    data: {
      title: 'Правила',
    }
  },
  {
    path: 'rules/edit',
    canActivate: [AuthGuards],
    component: RulesEditComponent,
    data: {
      title: 'Правила',
    }
  },
  {
    path: 'rules/edit/:id',
    canActivate: [AuthGuards],
    component: RulesEditComponent,
    data: {
      title: 'Правила',
    }
  },
  {
    path: 'orders',
    canActivate: [AuthGuards],
    component: OrdersComponent,
    data: {
      title: 'Клиенты',
    }
  },
  {
    path: 'orders/edit/:id',
    canActivate: [AuthGuards],
    component: OrdersEditComponent,
    data: {
      title: 'Клиенты',
    }
  },
  {
    path: 'orders/edit',
    canActivate: [AuthGuards],
    component: OrdersEditComponent,
    data: {
      title: 'Клиенты',
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppsRoutingModule { }
