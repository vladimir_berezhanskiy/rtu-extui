import {AfterViewInit, Component, OnInit, Output, ViewChild, EventEmitter} from '@angular/core';

import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { DialogBoxComponent } from '../dialog-box/dialog-box.component';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {UsersDataList} from '../../shared/interfaces/user.interfaces';
import {AppsService} from '../../shared/services/app.service';
import {LoadingService} from '../../shared/services/loading.service';
import {AuthenticationService} from '../../shared/services/auth.service';
import {ToastrService} from "ngx-toastr";

@Component({
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit, AfterViewInit {
  subscribe: any;
  loading$ = this.loader.loading$;
  displayedColumns: string[] = ['id', 'login', 'name', 'surname', 'email', 'role', 'status', 'action'];
  dataSource?: MatTableDataSource<UsersDataList>;

  @ViewChild(MatTable, {static: true}) table: MatTable<any>;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(public dialog: MatDialog, private appsService: AppsService, public loader: LoadingService,
              public AuthService: AuthenticationService, private toastr: ToastrService) {
    this.dataSource = new MatTableDataSource();
  }

  openDialog(action, obj): void {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data: obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result?.event === 'Delete'){
        this.deleteRowData(result?.data);
      }
    });
  }
  // tslint:disable-next-line:variable-name
  deleteRowData(row_obj): void {
    this.dataSource.data = this.dataSource.data.filter((value, key) => {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      return value.id !== row_obj.id;
    });
    this.table.renderRows();
    this.appsService.deleteUser(row_obj.id).subscribe(result => {
      this.toastr.info(`Пользователь с ID ${row_obj.id} удален`, 'Info');
    });
  }

  applyFilter(filterValue?: string): void {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    setTimeout(() => {
      this.dataSource.filter = filterValue;
    }, 1000);
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.subscribe = this.appsService.getUsers().subscribe(result => {
      // @ts-ignore
      this.dataSource?.data = result.list || [];
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
}
