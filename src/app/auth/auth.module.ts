import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthRoutingModule } from './auth-routing.module';
import {SignInComponent} from './sign-in/sign-in.component';
import {RegistrationComponent} from './registration/registration.component';
import {ForgotPassComponent} from './forgot-pass/forgot-pass.component';
import {NotFoundComponent} from '../pages/not-found/not-found.component';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../shared/shared.module';



@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AuthRoutingModule,
    RouterModule,
    SharedModule
  ],
  declarations: [
    SignInComponent,
    RegistrationComponent,
    ForgotPassComponent,
    NotFoundComponent
  ],
  providers: []
})

export class AuthenticationModule {}
