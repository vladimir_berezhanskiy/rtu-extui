import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SignInComponent} from './sign-in/sign-in.component';
import {RegistrationComponent} from './registration/registration.component';
import {NotFoundComponent} from '../pages/not-found/not-found.component';
import {ForgotPassComponent} from './forgot-pass/forgot-pass.component';


const routes: Routes = [
/*  {
    path: '',
    redirectTo: '/auth/login',
    pathMatch: 'full',
  },*/
  {
    path: 'login',
    component: SignInComponent,
    data: {
      title: 'Login'
    }
  },
  {
    path: 'register',
    component: RegistrationComponent,
    data: {
      title: 'Register'
    }
  },
  {
    path: 'forgot',
    component: ForgotPassComponent,
    data: {
      title: 'Forgot password'
    }
  },
  {
    path: '**',
    component: NotFoundComponent,
    data: {
      title: 'Error 1'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule { }
