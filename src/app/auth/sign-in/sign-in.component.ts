import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../shared/services/auth.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  year: Date;
  loginForm: FormGroup;

  submitForm(): void {
    if (this.loginForm.invalid) {
      return;
    }
    const user = {
      action: 'auth',
      obj: 'User',
      action_id: '123',
      params: {
        login: this.loginForm.value.username,
        password: this.loginForm.value.password
      }
    };
    this.AuthService.login(user).subscribe((data) => {
        if (data) {
          this.router.navigate(['/', 'dashboard']);
          this.toastr.success('Вход выполнен');
        }
      },
      error => {
        /*error = /!*error.error.message || error.statusText*!/;*/
        this.toastr.error(`Неверный логин или пароль`);
      });
  }

  constructor(private fb: FormBuilder, private AuthService: AuthenticationService, private router: Router,
              private toastr: ToastrService) {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
    });
  }

  ngOnInit(): void {
    if (this.AuthService.currentUserValue) {
      this.router.navigate(['/', 'dashboard']);
    }
    this.year = new Date();
  }
}
