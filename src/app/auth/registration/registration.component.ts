import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../shared/services/auth.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private fb: FormBuilder, private AuthService: AuthenticationService, private router: Router,
              private toastr: ToastrService) {
    this.loginForm = new FormGroup({
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(5)
      ]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      gender: new FormControl('', [
        Validators.required
      ])
      /*userPhone: new FormControl('', Validators.pattern("[0-9]{10}"))*/
    });
  }

  ngOnInit(): void {
  }

  submitForm(): void {
    if (this.loginForm.invalid) {
      return;
    }
    const user = {
      username: this.loginForm.value.username,
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
      gender: this.loginForm.value.gender,
    };
    this.AuthService.register(user).subscribe(() => {
        this.router.navigate(['/auth/', 'login']);
        this.toastr.success('Registration completed successfully', 'Info');
      },
      error => {
        error = error.error.message || error.statusText;
        this.toastr.error(`${error}`, 'Error');
      });
  }
}
