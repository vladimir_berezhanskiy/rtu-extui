import { NgModule } from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {LayoutsComponent} from './layouts/layouts.component';
import {WelcomComponent} from './pages/welcom/welcom.component';
import {NotFoundComponent} from './pages/not-found/not-found.component';
import {CommonLayout_ROUTES} from './layouts/routes/layouts.routes';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full',
  },
  {
    path: 'auth',
    loadChildren: () => import('../app/auth/auth.module').then(m => m.AuthenticationModule),
  },
  {
    path: '',
    component: LayoutsComponent,
    children: CommonLayout_ROUTES
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
