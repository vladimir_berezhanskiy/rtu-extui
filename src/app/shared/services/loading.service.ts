import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {NgxSpinnerService} from 'ngx-spinner';

@Injectable({providedIn: 'root'})
export class LoadingService {
  // tslint:disable-next-line:variable-name
  public _loading = new BehaviorSubject<boolean>(false);
  public readonly loading$ = this._loading.asObservable();
  public isLoading = false;

  constructor(private spinner: NgxSpinnerService) {}

  show(): void {
    //console.log('show');
    this.spinner.show();
    this.isLoading = false;
    this._loading.next(true);
  }

  hide(): void {
    //console.log('hide');
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
      this.isLoading = true;
      this._loading.next(false);
    }, 500);
  }
}
