import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Router} from '@angular/router';
import { environment } from '../../../environments/environment';
import { User } from '../interfaces/user';
const AUTH_API_URL = environment.apiUrl;

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  public get isLogin(): boolean {
    return localStorage.getItem('currentUser') ? true : false;
  }

  login(user: any): Observable<User> {
    return this.http.post<User>(`${AUTH_API_URL}`, user, {})
      .pipe(map(data => {
        if (data && data.body.token) {
          localStorage.setItem('currentUser', JSON.stringify(data));
          this.currentUserSubject.next(data);
        }
        return data;
      }));
  }

  register(user: any): Observable<any> {
    return this.http.post<User>(`${AUTH_API_URL}`, user);
  }

  logout(): void {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    this.router.navigate(['/auth/', 'login']);
  }
}
