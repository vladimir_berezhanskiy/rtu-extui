import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import {User} from '../interfaces/user';
import {NumbersDataList, OrdersDataList, RulesDataList, UsersDataList} from '../interfaces/user.interfaces';
const AUTH_API_URL = environment.apiUrl;

@Injectable({providedIn: 'root'})

export class AppsService {
  constructor(private http: HttpClient) {}



  // tslint:disable-next-line:variable-name max-line-length
   addUser(login: string, email: string, password: string, name: string, status: number, role_id: number, surname: string): Observable<UsersDataList> {
     const body = {
       action: 'append',
       obj: 'User',
       action_id: '123',
       params: {
         login,
         email,
         password,
         name,
         status,
         role_id,
         surname
       }
     };
     return this.http.post<any>(`${AUTH_API_URL}`, body);
   }

   getUsers(): Observable<UsersDataList> {
    const body = {
      action: 'list',
      obj: 'User',
      action_id: '123',
      params: {
        domain_id: 153
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }

  getUser(id: number): Observable<UsersDataList> {
    const body = {
      action: 'get',
      obj: 'User',
      action_id: '123',
      params: {
        id
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }

  getOrder(id: number): Observable<OrdersDataList> {
    const body = {
      action: 'get',
      obj: 'Order',
      action_id: '123',
      params: {
        id
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }

  deleteUser(id: number): Observable<UsersDataList> {
    const body = {
      action: 'delete',
      obj: 'User',
      action_id: '123',
      params: {
        id
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }

  deleteOrder(id: number): Observable<OrdersDataList> {
    const body = {
      action: 'delete',
      obj: 'Order',
      action_id: '123',
      params: {
        id
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }


  deleteNumber(id: number): Observable<OrdersDataList> {
    const body = {
      action: 'delete',
      obj: 'NumberDirectory',
      action_id: '123',
      params: {
        id
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }

  // tslint:disable-next-line:max-line-length
  updateUser(id: number, email: string, login: string, name: string, surname: string, status: number, password?: string): Observable<UsersDataList> {
    if (password) {
      const body = {
        action: 'update',
        obj: 'User',
        action_id: '123',
        params: {
          id,
          email,
          login,
          name,
          surname,
          status,
          password
        }
      };
      return this.http.post<any>(`${AUTH_API_URL}`, body);
    } else {
      const body = {
        action: 'update',
        obj: 'User',
        action_id: '123',
        params: {
          id,
          email,
          login,
          name,
          surname,
          status,
        }
      };
      return this.http.post<any>(`${AUTH_API_URL}`, body);
    }
  }

  // @ts-ignore
  getNumbers(offset?: number, sort?: string, limit?: number, filter: any): Observable<NumbersDataList> {
    const body = {
      action: 'list',
      obj: 'NumberDirectory',
      action_id: '123',
      offset: offset * 100,
      sort: {
        value: sort,
      },
      limit,
      ...filter,
      params: {
      },
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }

  getNumber(id: number): Observable<NumbersDataList> {
    const body = {
      action: 'get',
      obj: 'NumberDirectory',
      action_id: '123',
      params: {
        id
      },
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }

  // @ts-ignore
  getOrders(offset?: number, sort?: string, limit?: number, filter?: any): Observable<OrdersDataList> {
    const body = {
      action: 'list',
      obj: 'Order',
      action_id: '123',
      offset: offset * 100,
      sort: {
        name: sort,
      },
      limit,
      params: {
      },
      ...filter
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }

  // tslint:disable-next-line:variable-name
  addOrders(customer_name: string, name: string, desc: string, status: number): Observable<OrdersDataList> {
      const body = {
        action: 'append',
        obj: 'Order',
        action_id: '123',
        params: {
          customer_name,
          name,
          desc,
          status,
        }
      };
      return this.http.post<any>(`${AUTH_API_URL}`, body);
    }

  // tslint:disable-next-line:variable-name
  updateOrders(id: number, customer_name: string, name: string, desc: string, status: number): Observable<OrdersDataList> {
    const body = {
      action: 'update',
      obj: 'Order',
      action_id: '123',
      params: {
        id,
        customer_name,
        name,
        desc,
        status,
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }

  updateNumber(id: number, value: string, desc: string, reserved_until: any, block_until: any,
               order_id: number, in_rr_id: number, out_rr_id: number): Observable<OrdersDataList> {
    const body = {
      action: 'update',
      obj: 'NumberDirectory',
      action_id: '123',
      params: {
        id,
        value,
        desc,
        reserved_until,
        block_until,
        order_id,
        in_rr_id,
        out_rr_id
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }

  addNumber(value: string, desc: string, reserved_until: any, block_until: any,
               order_id: number, in_rr_id: number, out_rr_id: number): Observable<NumbersDataList> {
    const body = {
      action: 'append',
      obj: 'NumberDirectory',
      action_id: '123',
      params: {
        value,
        desc,
        reserved_until,
        block_until,
        order_id,
        in_rr_id,
        out_rr_id
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }

  getRules(id?: number): Observable<RulesDataList> {
    const body = {
      action: 'list',
      obj: 'RouteRule',
      action_id: '123',
      params: {
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }

  getRule(id?: number): Observable<RulesDataList> {
    const body = {
      action: 'get',
      obj: 'RouteRule',
      action_id: '123',
      params: {
        id
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }

  addRule(name: string, contact: string): Observable<RulesDataList> {
    const body = {
      action: 'append',
      obj: 'RouteRule',
      action_id: '123',
      params: {
        name,
        contact
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }

  updateRule(id: number, name: string, contact: string): Observable<RulesDataList> {
    const body = {
      action: 'append',
      obj: 'RouteRule',
      action_id: '123',
      params: {
        id,
        name,
        contact
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }

  deleteRule(id: number): Observable<RulesDataList> {
    const body = {
      action: 'delete',
      obj: 'RouteRule',
      action_id: '123',
      params: {
        id,
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }


  countOrders(): Observable<OrdersDataList> {
    const body = {
      action: 'list',
      obj: 'Order',
      action_id: '123',
      limit: 1,
      params: {
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }

  countUsers(): Observable<UsersDataList> {
    const body = {
      action: 'list',
      obj: 'User',
      action_id: '123',
      limit: 1,
      params: {
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }
  countNumbers(): Observable<NumbersDataList> {
    const body = {
      action: 'list',
      obj: 'NumberDirectory',
      action_id: '123',
      limit: 1,
      params: {
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }

  countRules(): Observable<NumbersDataList> {
    const body = {
      action: 'list',
      obj: 'RouteRule',
      action_id: '123',
      limit: 1,
      params: {
      }
    };
    return this.http.post<any>(`${AUTH_API_URL}`, body);
  }
}
