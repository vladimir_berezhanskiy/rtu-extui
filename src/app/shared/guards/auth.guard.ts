import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthenticationService } from '../services/auth.service';
import {ToastrService} from 'ngx-toastr';

@Injectable({providedIn: 'root'})
export class AuthGuards implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private toastr: ToastrService
  ) {
  }

  // tslint:disable-next-line:typedef
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.authenticationService.currentUserValue;
    if (currentUser && currentUser?.body?.token) {
      return true;
    }
    this.toastr.info('Сессия устарела, авторизуйтесь');
    this.authenticationService.logout();
      /*window.location.reload();*/
    return false;
  }
}
