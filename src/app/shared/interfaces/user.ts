export class User {
  action: string;
  obj: string;
  // tslint:disable-next-line:variable-name
  action_id: string;
  code?: string;
  params?: {
    domain_id: number
  };
  body?: {
    exp?: string
    user_id?: number
    user_name?: string,
    user_login?: string,
    role_id?: number
    role_name?: string
    role_type?: number
    role_perms?: {}
    token?: string
  };
}
