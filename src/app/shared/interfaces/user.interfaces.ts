export interface UserInterfaces {
  action: string;
  obj: string;
  action_id: string;
  code?: string;
  params?: {
    domain_id: number
  };
  body?: {
    exp?: string
    user_id?: number
    user_name?: string,
    user_login?: string,
    role_id?: number
    role_name?: string
    role_type?: number
    role_perms?: {}
    token?: string
  };
}
export interface UsersDataList {
  body?: any;
  avatar: string;
  domain_id: string;
  id: number;
  email: string;
  login: string;
  name: string;
  role_id: number;
  status: string;
  total_count: number;
}

export interface NumbersDataList {
  action: string;
  action_id: string;
  obj: string;
  body: any;
  code: number;
  id: number;
  total_count: number;
  list: [{
    id: number;
    value: number;
    reserved_until: string;
    block_until: string;
    desc: string;
    order_id: string;
    in_rr_id: number;
    out_rr_id: number;
  }];
}

export interface OrdersDataList {
  limit?: number;
  offset?: number;
  id?: number;
  customer_name?: string;
  name?: string;
  desc?: string;
  status?: number;
  action: string;
  action_id: string;
  obj: string;
  code: number;
  total_count?: number;
  body?: {
    id?: number,
    customer_name?: string,
    name?: string,
    desc?: string,
    status?: number;
  };
  list: [{
    id: number,
    customer_name: string,
    name: string,
    desc: string,
    status: number;
  }];
}

export interface RulesDataList {
  action: string;
  action_id: string;
  obj: string;
  code: number;
  id: number,
  name: string,
  contact: string,
  list: [{
    id: number,
    name: string,
    contact: string,
  }];
}
