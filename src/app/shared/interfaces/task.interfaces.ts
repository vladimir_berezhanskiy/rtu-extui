import {Priority} from './priority.interfaces';
import {Category} from './category.interfaces';

export class TaskInterfaces {
  id: number;
  title: string;
  completed: boolean;
  priority?: Priority;
  category?: Category;
  date?: Date;
}
