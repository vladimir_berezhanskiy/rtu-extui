export class Priority {
  id: number;
  title: string;
  color: string;
}
