import { Pipe, PipeTransform } from '@angular/core';



@Pipe({
  name: 'dateEpoch'
})

export class DateEpochPipe implements PipeTransform {
  transform(value: number | undefined, arg?: number | undefined): string {
    if (value) {
      const myDate = new Date( value * 1000);
      // @ts-ignore
      return myDate;
    }
    return '-';
  }
}
