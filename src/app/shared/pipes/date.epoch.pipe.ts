import { Pipe, PipeTransform } from '@angular/core';



@Pipe({
  name: 'epochDate'
})

export class EpochDatePipe implements PipeTransform {
  transform(value: number | undefined, arg?: number | undefined): string {
    const options = {
      day: 'numeric',
      month: '2-digit',
      year: 'numeric',
    };
    if (value) {
      const myDate = new Date( value * 1000);
      return myDate.toLocaleString('ru-RU', options);
    }
    return '-';
  }
}
