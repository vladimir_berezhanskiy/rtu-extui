import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';

import { AuthenticationService } from '../services/auth.service';
import {catchError, finalize, tap} from 'rxjs/operators';
import {LoadingService} from '../services/loading.service';
import {ToastrService} from 'ngx-toastr';
import {migrateLegacyGlobalConfig} from '@angular/cli/utilities/config';

@Injectable({providedIn: 'root'})
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService, private loader: LoadingService,
              private toastr: ToastrService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.loader.show();
    const currentUser = this.authenticationService.currentUserValue;
    if (currentUser && currentUser.body.token) {
      //console.log(currentUser);
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${currentUser.body.token}`
        }
      });
    }
    return next.handle(request).pipe(
      tap(data => {
        // @ts-ignore
        switch (data?.body?.code) {
          case 407: {
            this.authenticationService.logout();
            /*return;
            this.toastr.warning('Сессия устарела, авторизуйтесь');*/
          }
          // tslint:disable-next-line:no-switch-case-fall-through
          case 500: {
            this.toastr.warning('Произошла ошибка, повторите запрос позже');
            return;
          }
        }
      }),
      finalize(() => {
        this.loader.hide();
      }));
  }
}
